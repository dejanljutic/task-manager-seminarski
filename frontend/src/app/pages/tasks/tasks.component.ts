import { Component, OnInit } from '@angular/core';
import { TasksService } from '../../services/tasks.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
  tasks = [];
  hidden = false;

  constructor(private tasksService: TasksService) { }

  ngOnInit() {
    this.getTasks();
  }

  hideCompletedTasks() {
    const currentLength = this.tasks.length;
    this.tasks = this.tasks.filter(task => !task.completed);
    
    if (currentLength !== this.tasks.length)
      this.hidden = true;
  }

  showCompletedTasks() {
    this.getTasks();
    this.hidden = false;
  }

  getTasks() {
    this.tasksService.getTasks()
      .subscribe(
        res => {
          this.tasks = res;
          
          if (this.hidden) 
            this.hideCompletedTasks();
        },
        err => {
          console.log(err);
        }
      );
  }

  toggleCompleted(task) {
    const updates = { completed : !task.completed }
    this.tasksService.updateTask(task._id, updates)
      .subscribe(
        res => {
          this.getTasks();
        },
        err => {
          console.log(err);
        }
      );
  }

  changeDescription(task) {
    const updates = { description : task.description}
    this.tasksService.updateTask(task._id, updates)
      .subscribe(
        res => {
          this.getTasks();
          alert("Uspešno sačuvane izmene.");
        },
        err => {
          console.log(err);
        }
      );
  }

  deleteTask(task) {
    this.tasksService.deleteTask(task._id) 
      .subscribe (
        res => {
          this.getTasks();
          alert("Uspešno obrisan task.");
        },
        err => {
          console.log(err);
        }
      );
  };

}
