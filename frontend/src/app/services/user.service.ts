import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private userUrl = 'http://localhost:3000/users/me';
  private avatarUrl = 'http://localhost:3000/users/me/avatar';

  constructor(private http: HttpClient) { }

  updateUser(updates) {
    return this.http.patch<any>(this.userUrl, updates);
  }

  deleteUser(user) {
    return this.http.delete<any>(this.userUrl, user);
  }

  uploadAvatar(avatar) {
    return this.http.post<any>(this.avatarUrl, avatar);
  }

  deleteAvatar(user) {
    return this.http.delete<any>(this.avatarUrl, user)
  }
}
